import requests
from bs4 import BeautifulSoup
import re
import urllib
import os
from functools import reduce
from flask import Flask, render_template, send_from_directory

# utillity functions

def pipe(*xs):
    return reduce(lambda x, f: f(x), xs)

# text transformers

def paragraphnize(main_text):
    pattern = r"　(.*)<br/>"
    replacer = lambda matched: "<p>{}</p>".format(matched.group(1))
    return re.sub(pattern, replacer, main_text.replace('\n「', '\n　「')).replace('<br/>', '')

def remove_hr(text):
    return text.replace('<hr/>\n<br/>', '')

def remove_trailing_yaku(translator):
    return translator[:-1] if translator and translator[-1] == '訳' else translator

def remove_multi_br(text):
    pattern = r'(<br ?\/?>\n?){2,}'
    replacer = lambda _matched: "<br>"
    return re.sub(pattern, replacer, text)

def replace_multi_str(text):
    return text.replace('／＼', '〱').replace('／&Prime;＼', '〲')

def replace_paragraph_separator(text):
    pattern = r"<p>　*([(&cir;)零〇壱一弐二参三肆四伍五陸六漆七捌八玖九拾十陌百阡千萬万億兆]+)<\/p>"
    replacer = lambda matched: "<p class='separator'>{}</p>".format(matched.group(1))
    return re.sub(pattern, replacer, text)

def replace_translators_notes(text):
    pattern = r"<p>　*(訳注)<\/p>"
    replacer = lambda matched: "<h3 class='translators_notes'>{}</h3>".format(matched.group(1))
    return re.sub(pattern, replacer, text)

def combine_symbols(*symbols):
    pattern = r"[{}]".format(''.join(symbols))
    replacer = lambda matched: "<span class='combine'>{}</span>".format(matched.group(0))
    return lambda text: re.sub(pattern, replacer, text)

def combine_digits(n=3):
    # TODO: もっとよく考える
    pattern = r"(?<![a-zA-Z\.\=\~\/\<\>\,\?\!\#\$])\d+(?![a-zA-Z\.\=\~\/\<\>\,\?\!\#\$]+)"
    def replacer(matched):
        digits = matched.group(0)
        if len(digits) <= n:
            return "<span class='combine'>{}</span>".format(digits)
        return digits
    return lambda text: re.sub(pattern, replacer, text)

def remove_hyoukinitsuite(text):
    return text.replace('●表記について<br/>\n', '')

def replace_gaiji(soup):
    for img in soup.select('img.gaiji'):
        src = img.get('src')
        img['src'] = src.replace('../../../', 'https://www.aozora.gr.jp/')
    return soup

# private tools

def card_href(file_path):
    url = urllib.parse.urlparse(file_path)
    base, _ = os.path.splitext(os.path.basename(url.path))
    card_id, _ = base.split('_', 2)
    paths = url.path.split('/')
    new_path = paths[:-2] + ["card{}.html".format(card_id)]
    new_url = url._replace(path='/'.join(new_path))
    return urllib.parse.urlunparse(new_url)

# main converter

def convert(url):
    res = requests.get(url)
    soup = pipe(BeautifulSoup(res.content, 'html.parser'), replace_gaiji)
    title = soup.select_one(".title").get_text()
    subtitle = soup.select_one(".subtitle")
    subtitle = subtitle.get_text() if subtitle else ""
    author = soup.select_one(".author").get_text()
    translator = soup.select_one(".translator")
    translator = translator.get_text() if translator else ""
    main_text = soup.select_one(".main_text").decode_contents(formatter="html")
    after_text = soup.select_one(".after_text")
    after_text = after_text.decode_contents(formatter="html") if after_text else ""
    info = soup.select_one(".bibliographical_information")
    info = info.decode_contents(formatter="html") if info else after_text
    notes = soup.select_one(".notation_notes").decode_contents(formatter="html")
    return {
        "title": title,
        "subtitle": subtitle,
        "author": author,
        "translator": pipe(translator, remove_trailing_yaku),
        "main_text": pipe(
            main_text,
            paragraphnize,
            replace_multi_str,
            replace_paragraph_separator,
            replace_translators_notes
        ),
        "info": pipe(info, remove_hr, remove_multi_br),
        "notes": pipe(notes, remove_hr, remove_hyoukinitsuite),
        "card_href": card_href(url),
        "url": url.replace('https://www.aozora.gr.jp/', 'https://aozora-format.herokuapp.com/'),
        "original_url": url,
    }

# server settings

app = Flask(__name__)

@app.route('/')
def index():
    return send_from_directory('static', 'index.html')

@app.route('/cards/<path:url>')
def root(url):
    url = "https://www.aozora.gr.jp/cards/{}".format(url)
    return render_template("template.j2", **convert(url))
