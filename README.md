```
Herokuの料金体制変更によりサービスの提供を行うことが金銭的に難しくなったため、停止中です。
```

# 縦書きで読む青空文庫

青空文庫を縦書きで読むことができます。
インストールは不要です。
スマートフォンやタブレットにも対応しています。

## 使い方

[トップページ（縦書きで読む青空文庫.jp）](https://縦書きで読む青空文庫.jp/)のインプットボックスに各XHTMLファイルのURLを貼りつけて「縦書きで読む」ボタンを押す。

もしくは、各XHTMLファイルのURLのうち`https://www.aozora.gr.jp/`を`https://縦書きで読む青空文庫.jp/`に変えるだけ。

### 例

『[岸田國士 遺憾の弁 ――芥川賞（第二十四回）選後評――](https://www.aozora.gr.jp/cards/001154/files/44829_40307.html)』のURLは`https://www.aozora.gr.jp/cards/001154/files/44829_40307.html`なので`https://縦書きで読む青空文庫.jp/cards/001154/files/44829_40307.html`にアクセスすれば縦書きで読める。

[『岸田國士 遺憾の弁 ――芥川賞（第二十四回）選後評――』を縦書きで読む](https://縦書きで読む青空文庫.jp/cards/001154/files/44829_40307.html)

## ブックマークレット

Snippet https://gitlab.com/fj68/aozora-format/-/snippets/2230741 に置いてあります。

## For Developpers

Assuming you have installed followings:

 - Git
 - Pyenv (recommended to ensure Python version)

```sh
git clone https://gitlab.com/fj68/aozora-format.git
cd aozora-format
python3 -m venv .venv
source .venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
flask run
```

## License

MIT
